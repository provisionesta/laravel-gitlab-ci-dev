# GitLab CI Pipeline for Laravel Development

## Overview

### Problem Statement

It can take tens or hundreds of hours to get pipeline testing and deployments set up, and there are many ways to solve the problem. We want to make it easy for developers to worry less about how to build their pipelines and safely ship their deployments to production.

This repository provides production hardened example files with a turn key getting started guide for using [GitLab CI/CD pipelines](https://docs.gitlab.com/ee/ci/) for [Laravel (PHP) projects](https://laravel.com/docs/11.x) to perform testing and build container images that are ready to use in production container infrastructure deployments.

### Benefits

- [Composer](https://getcomposer.org/) and [Node Package Manager (NPM)](https://www.npmjs.com/) dependency installation validation and security scanning
- Code style linting using [PSR-12](https://www.php-fig.org/psr/psr-12/) ([PHPStan](https://phpstan.org/), [Larastan](https://github.com/larastan/larastan), [PHP-CS](https://github.com/PHPCSStandards/PHP_CodeSniffer/))
- Unit testing with [PestPHP](https://pestphp.com/), an alternative to PHPUnit
- GitLab [Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) Reports
- GitLab [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) Scanning
- GitLab [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- GitLab [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- Docker Image Builds
- Docker [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- Pushing images to GitLab [Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- Security scan results can be seen on the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/) or [Vulnerability Dashboard](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/)

### Maintainers

This is maintained by the open source community and is not maintained by any company. Please use at your own risk and create merge requests for any bugs that you encounter.

| Name | GitLab Handle | Email |
|------|---------------|-------|
| [Jeff Martin](https://www.linkedin.com/in/jeffersonmmartin/) | [@jeffersonmartin](https://gitlab.com/jeffersonmartin) | `provisionesta [at] jeffersonmartin [dot] com` |

## Getting Started

### GitLab Project

If you haven't already, [create a GitLab project](https://docs.gitlab.com/ee/user/get_started/get_started_projects.html) and commit your Laravel application to the repository.

These instructions assume that you are starting with a [blank Laravel 11 project](https://laravel.com/docs/11.x/installation#creating-a-laravel-project), so if you have an existing project, some steps may already be completed or need to be adjusted.

### Add files

In the `examples/` folder, copy each of these files into your repository. You can use the GitLab UI to upload files, GitLab Web IDE to commit your changes, or perform these steps in your locally cloned repository and commit the changes.

If you have existing files with the same name, you will need to use your discretion to determine whether to modify your existing file, copy it to a backup location, or replace it entirely. When working with existing `gitlab-ci.yml` files, it is expected to need multiple merge requests to get your pipelines to pass with your changes.

- `examples/tests/Pest.php`
- `.gitlab-ci.yml`
- `Dockerfile`
- `phpstan.neon`

### Add Larastan Composer package

```bash
composer require larastan/larastan:^2.0 --dev
```

### Add PestPHP Composer package

This pipeline uses PestPHP and will remove PHPUnit. **If you want to keep using PHPunit**, you can skip these steps and comment out or remove the `pest` job from `.gitlab-ci.yml`.

- Docs: [PestPHP Installation](https://pestphp.com/docs/installation)
- Docs: [Migrating from PHPUnit guide](https://pestphp.com/docs/migrating-from-phpunit-guide)

```bash
composer remove phpunit/phpunit
composer require pestphp/pest --dev --with-all-dependencies
```

### Run Your Pipeline

Your pipeline will run automatically as you make changes to your repository with commits, merge requests, and tags.

- Docs: [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- Docs: [GitLab Jobs](https://docs.gitlab.com/ee/ci/jobs/)
- Docs: [GitLab Runner](https://docs.gitlab.com/runner/)

To validate that everything is working, you can manually run a pipeline now on your `main` branch.

1. In the GitLab UI, navigate to `Build > Pipelines`.

2. Click the **Run Pipeline** button in the top right corner.

3. Leave the branch set to `main`. You do not need to provide any variables here.

4. Click the **Run Pipeline** button.

5. You will see the jobs start to run. Our `.gitlab-ci.yml` file has specified how each job should run, including any dependencies that it needs to start running. You can visually see this by clicking on the **Group jobs by `Job dependencies`** button, then toggle the **Show dependencies** option.

    ![New Pipeline](images/first-pipeline-started.png)

    ![Pipeline Progress](images/first-pipeline-progress.png)

6. You can click on the name of each job to view the CI job log output.

7. Within a few minutes, your pipeline jobs should be complete. You can see the time that each job took in the top right corner of the details page for each job.

8. You will notice that the `code_quality` job can take 5+ minutes. To speed up your pipelines, this job only runs on the `main` branch, `releases`, and tags. This job is hidden from any development, feature and fix branches. If things are working well during development branches, each pipeline should take less than 3 minutes to complete.

### View Your Container Image

All Docker images are stored in the GitLab Container Registry that you can see in the GitLab UI and access similar to how you would use Docker Hub.

- Docs: [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- Docs: [Authenticate with Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html)

1. In the GitLab UI, navigate to `Deploy > Container Registry`.

2. You will see your project name listed. **Click on the project name** to view all of the tags.

3. Locate the `latest` tag and click on the clipboard icon to copy the path.

4. **Paste this path** somewhere (ex. empty text file, new browser tab URL, chat message, etc) to see what it looks like. If you already use Docker Hub or another container registry, this will feel very familiar. If this is a private project, the container registry is also private and you will need to authenticate when using the image in your deployments.

    ```plain
    registry.gitlab.com/provisionesta/my-cool-project:latest
    ```

    <img src="images/container-registry.png" width="300px;">

### Configure Container Registry Cleanup Policy

Keep in mind that container images are usually several hundred megabytes (MB) in size, so they can quickly consume quotas on your GitLab account. You should configure the Cleanup Policy so that any image besides `latest` and `v{x.y.z}` is only kept for 14 days.

- Docs: [Container Registry Cleanup Policy](https://docs.gitlab.com/ee/user/packages/container_registry/reduce_container_registry_storage.html#cleanup-policy)

1. In the GitLab UI, navigate to `Settings > Packages and Registries`.

2. Add or edit the **Cleanup Policies (Rules)**.

3. Toggle the **Enabled** flag to be on.

4. Set **Run Cleanup** to `Every day`.

5. Set **Keep these tags** to `1 tag per image name` and tags matching `v.+`.

6. Set **Remove these tags** to `14 days` and tags matching `.*`.

7. Save your changes.

## Usage Instructions

This pipeline assumes that you are using a continuous delivery model with a default branch (usually `main`) with tagged releases. We are biased towards using GitLab Flow, however you can use whatever simple or complex workflow that you prefer. It has not been hardcoded for `develop`, `feature` or `hotfix` branches, however you can modify any job rules as needed using existing release branch conditionals as inspiration.

- Overview: [GitLab Flow Best Practices](https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/)
- Docs: [Managing your Code](https://docs.gitlab.com/ee/user/get_started/get_started_managing_code.html)
- Docs: [Create a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- Docs: [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- Docs: [GitLab Jobs](https://docs.gitlab.com/ee/ci/jobs/)
- Blog: [GitLab Flow with GitLab Duo](https://about.gitlab.com/blog/2023/07/27/gitlab-flow-duo/)

```mermaid
gitGraph
   commit
   commit
   branch add-cool-feature
   checkout add-cool-feature
   commit
   commit
   checkout main
   merge add-cool-feature
   branch fix-another-feature
   checkout fix-another-feature
   commit
   checkout main
   merge fix-another-feature
   commit
   commit tag:"1.2"
   branch release/1.2.1
   checkout release/1.2.1
   commit
   branch hotfix/ui-bug
   checkout hotfix/ui-bug
   commit
   checkout release/1.2.1
   merge hotfix/ui-bug tag:"1.2.1"
   checkout main
   merge release/1.2.1
```

### Create a Patch Release

The steps to create a patch release from a tag are a bit tricky since we do not use the `main` branch that is the default when creating a branch or merge request.

1. Navigate to **Code > Branches** and click **New Branch**.

    - **Branch Name:** `release/{x.y.z}`
    - **Create from:** Tags > `{x.y}`

2. Save the branch.

3. Click the **New branch** button again.

    - **Branch Name:** `hotfix/{description-of-problem}`
    - **Create from:** `release/{x.y.z}`

4. At the top of the page, click the alert message **Create merge request** button.

5. At the top of the new merge request page, click the **Change branches**

    - Source branch: `hotfix/{description-of-problem}`
    - Target branch: `release/{x.y.z}`

6. Click **Compare branches and continue**.

7. Set the **Title** to `Draft: {Description of Problem}`

8. Assign the merge request to yourself.

9. Click the **Create merge request**.

10. Proceed like you normally would with committing code to this branch and getting approvals to merge this branch in.

11. It is important to **not to delete the branch when merging** the patch release in. This is a checkbox on the merge request page. After the patch release has been merged, create a new merge request to merge the `release/{x.y.z}` branch into `main`. You may need to resolve merge conflicts.

## Pipelines

All helpful test stage jobs run on any commit, regardless of the branch.

The Docker jobs run on the `main` and `release/*` branches, and any Git tag pipelines. A `sha` image is created for the commit on each of these branches

### Development or Feature Branch

![Pipeline](images/pipeline-dev-branch.png)

### Default Branch (`main`)

![Pipeline](images/pipeline-default-branch.png)

### Release Branch

![Pipeline](images/pipeline-release-branch.png)

### Tagged Release

![Pipeline](images/pipeline-tag.png)

## Related Docs

- Emojis: [https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md](https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md)
- CI Docs: [https://docs.gitlab.com/ee/ci/yaml/](https://docs.gitlab.com/ee/ci/yaml/)
- CI Variables: [https://docs.gitlab.com/ee/ci/variables/predefined_variables.html](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- CI Templates: [https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates)

## Credit

Thank you to the Laravel blog and open source community for inspiration and how to make each job work.

- [https://cloud.google.com/run/docs/quickstarts/build-and-deploy/deploy-php-service](https://cloud.google.com/run/docs/quickstarts/build-and-deploy/deploy-php-service)
- [https://dev.to/jackmiras/laravel-with-php7-4-in-an-alpine-container-3jk6](https://dev.to/jackmiras/laravel-with-php7-4-in-an-alpine-container-3jk6)
- [https://github.com/serversideup/docker-php](https://github.com/serversideup/docker-php)
- [https://github.com/harshalone/deploy-laravel-9-on-google-cloud-run](https://github.com/harshalone/deploy-laravel-9-on-google-cloud-run)
